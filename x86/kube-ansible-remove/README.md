1. 查看要重构的VM资源名
    a. 导入环境变量
       如：. yunjierc

    b. nova list

2. 根据VM 主机名对 VM 进行重新构建在cdsops/playbook/lib/oss下有一个rebuild.sh的重构脚本
  ./rebuild.sh <LONG_BASE_20G_v1.2_0812> <要进行重构的VM资源名称> 
 或
  ./rebuild.sh <LONG_BASE_100G_v1.1_0811> <要进行重构的VM资源名称> 

3. 查看重构的VM是否已经处于running状态

4. 以root用户登录一台VM，下载ansible的git仓库
  git clone https://username:password@git.oschina.net/leowa/cdsops.git

5. 运行ansible-install.sh 来安装和配置ansible：
  ./cdsops/kube/kube-ansible-roles/ansible-install.sh 

6. 修改ansible的inventory和 group_vars/all：
  a. cd cdsops/kube/kube-ansible-roles/
  b. 将inventory修改为对应的VM主机
  c. 修改group_vars/all 中的
     master_ip_port: x.x.x.x.:8080
     etcd_ip_port: x.x.x.x:4001

7. ansible-playbook -i <your inventory file>  setup.yml
