#!/bin/bash
#

wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo

rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key

yum install jenkins java-1.7.0-openjdk

sed -i 's/JENKINS_USER="jenkins"/JENKINS_USER="root"/g' /etc/sysconfig/jenkins

./install_jenkins_plugin.sh plugins /var/lib/jenkins/plugins

service jenkins start
