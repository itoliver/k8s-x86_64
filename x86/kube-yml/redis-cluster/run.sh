#!/bin/bash

kubectl create -f redis-master-rc.yaml
kubectl create -f redis-master-service.yaml
kubectl create -f redis-slave-rc.yaml
kubectl create -f redis-slave-service.yaml
