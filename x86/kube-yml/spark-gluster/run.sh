#!/bin/bash

kubectl create -f namespace-spark-cluster.yaml
kubectl create -f glusterfs-endpoints.yaml

kubectl create -f spark-master-controller.yaml
kubectl create -f spark-master-service.yaml
kubectl create -f spark-webui.yaml
kubectl create -f spark-worker-controller.yaml

