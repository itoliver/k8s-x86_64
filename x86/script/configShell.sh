#!/bin/sh
if [[ $# < 2 ]];then
    echo; echo "Usage: $0 targetFolder kube_master <namespace>"; echo;
    exit 1
fi

targetFolder=$1
kube_master=$2
namespace=${3:-spark-cluster}

mkdir $targetFolder
cp ./kube*.sh $targetFolder/
sed -i -e 's/__KUBE_MASTER/'$kube_master'/g' -e 's/__NAMESPACE/'$namespace'/g' $targetFolder/kube*.sh
