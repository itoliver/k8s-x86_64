#!/bin/bash
# get kube container ip


if [[ $# < 1 ]];then
    echo; echo "Usage: $0 pod_keywords <lines> <namespace>"; echo;
    exit 1
fi

KEYWORDS=$1

LINES=${2:-1000}

NAMESPACE=${3:-spark-cluster}

KUBESERVER=172.16.1.27

PODS=$(kubectl --server=${KUBESERVER} get pods --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1 | head -1)

if [ ! -z $PODS ];then

    kubectl --server=${KUBESERVER} logs $PODS --tail=$LINES --namespace=${NAMESPACE}

else
   echo; echo "Didn't find relevant containers, please check whether the specified the correct namespace"; echo
   exit 2
fi


