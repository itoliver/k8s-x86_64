#!/bin/bash
#

if [[ $# < 1 ]];then
    echo; echo "Usage: $0 pod_keywords <namespace>"; echo;
    exit 1
fi

KEYWORDS=$1

NAMESPACE=${2:-spark-cluster}

KUBE_MASTER=172.16.1.27


PODS=$(kubectl --server=${KUBESERVER} get pods --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1 | head -1)

if [ ! -z $PODS ];then

    #kubectl exec -it $PODS $APP --namespace=${NAMESPACE}
    cid=$(kubectl --server=${KUBESERVER} describe pods $PODS --namespace=${NAMESPACE} | grep 'Container ID:' | cut -d'/' -f3)

    # get container ip
    cip=$(kubectl --server=${KUBESERVER} describe pods $PODS --namespace=${NAMESPACE} | grep IP: | cut -d':' -f2 | xargs -n1)

    echo > /root/.ssh/known_hosts

    #sshpass -p zxcasd123 ssh-copy-id -i $cip &> /dev/null

    sshpass -p zxcasd123 ssh -o StrictHostKeyChecking=no root@$cip 

else 
    echo; echo "Didn't find relevant containers, please check whether the specified the correct namespace"; echo
    exit 2
fi
