#!/bin/bash
#

KEYWORDS=mongo

NAMESPACE=spark-cluster

APP=mongo

KUBESERVER=172.16.1.27


PODS=$(kubectl --server=${KUBESERVER} get pods --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1 | head -1)

if [ ! -z $PODS ];then

    #kubectl exec -it $PODS $APP --namespace=${NAMESPACE}
    cid=$(kubectl --server=${KUBESERVER} describe pods $PODS --namespace=${NAMESPACE} | grep 'Container ID:' | cut -d'/' -f3)

    # get container ip
    cip=$(kubectl --server=${KUBESERVER} describe pods $PODS --namespace=${NAMESPACE} | grep IP: | cut -d':' -f2 | xargs -n1)

    #ssh-copy-id -i $cip &> /dev/null

    #ssh $cip 

    mongo $cip
else 
    echo; echo "Didn't find relevant containers, please check whether the specified the correct namespace"; echo
    exit 2
fi


