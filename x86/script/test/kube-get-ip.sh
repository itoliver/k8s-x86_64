#!/bin/bash
# get kube container ip


if [[ $# < 1 ]];then
    echo; echo "Usage: $0 pod_keywords <namespace>"; echo;
    exit 1
fi

KEYWORDS=$1

NAMESPACE=${2:-spark-cluster}

KUBESERVER=172.16.1.27

PODS=$(kubectl --server=${KUBESERVER} get pods --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1 | head -1)

if [ ! -z $PODS ];then

    # get containter id
    cid=$(kubectl --server=${KUBESERVER} describe pods $PODS --namespace=${NAMESPACE} | grep 'Container ID:' | cut -d'/' -f3)

    # get container ip
    cip=$(kubectl --server=${KUBESERVER} describe pods $PODS --namespace=${NAMESPACE} | grep IP: | cut -d':' -f2 | xargs -n1)

    # get node ip address that cid
    nip=$(kubectl --server=${KUBESERVER} describe pods $PODS --namespace=${NAMESPACE} | grep 'Node:' | cut -d'/' -f2)

    echo "Container ID:     "$cid
    echo "Container IP:     "$cip
    echo "Container Node IP:"$nip

else
   echo; echo "Didn't find relevant containers, please check whether the specified the correct namespace"; echo
   exit 2
fi


