#!/bin/bash
# restart kubernetes container


if [[ $# < 1 ]];then
    echo; echo "Usage: $0 pod_keywords <namespace>"; echo;
    exit 1
fi

KEYWORDS=$1

NAMESPACE=${2:-spark-cluster}

KUBESERVER=172.16.1.27

PODS=$(kubectl --server=${KUBESERVER} get pods --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1)

for pod in ${PODS};do
    # get containter id
    cid=$(kubectl --server=${KUBESERVER} describe pods $pod --namespace=${NAMESPACE} | grep 'Container ID:' | cut -d'/' -f3)

    # get node ip address that cid
    nip=$(kubectl --server=${KUBESERVER} describe pods $pod --namespace=${NAMESPACE} | grep 'Node:' | cut -d'/' -f2)

    # restart container
    ssh $nip docker restart $cid
done
