#!/bin/bash
# restart kubernetes container


if [[ $# < 1 ]];then
    echo; echo "Usage: $0 pod_keywords <namespace>"; echo;
    exit 1
fi

KEYWORDS=$1

NAMESPACE=${2:-spark-cluster}

KUBESERVER=172.16.1.27

PODS=$(kubectl --server=${KUBESERVER} get pods --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1)

for pod in ${PODS};do

    kubectl --server=${KUBESERVER} delete $pod --namespace=${NAMESPACE} 

done
