#!/bin/bash
#
if [[ $# < 1 ]];then
     echo; echo "Usage: $0 service_keywords <namespace>"; echo;
    exit 1
fi

KEYWORDS=$1

NAMESPACE=${2:-spark-cluster}

KUBESERVER=172.16.1.27

RC=$(kubectl --server=${KUBESERVER} get rc --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1)

PODS=$(kubectl --server=${KUBESERVER} get pods --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1)

SVC=$(kubectl --server=${KUBESERVER} get svc --namespace=${NAMESPACE} | grep -i ${KEYWORDS} | cut -d' ' -f1)


echo "===== RC ====="
echo $RC

echo "===== PODS ====="
echo $PODS

echo "===== SVC ====="
echo $SVC
