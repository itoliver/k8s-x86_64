#!/bin/bash

# ../share/zabbix/alertscripts
 
to=$1
subject=$2
body=$3
 
cat <<EOF | mutt -s "$subject" "$to"
$body
EOF
