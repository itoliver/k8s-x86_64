#! /bin/bash
set +ex
VM=$2
BASE_IMAGE=$1

function fail() {
    echo "failed: $*"
    exit -1
}

[ -n "$VM" ] || fail "VM is not provided"
[ -n "$BASE_IMAGE" ] || fail "base image is not provided"
nohup bash -c "nova stop $VM; sleep 120; nova rebuild $VM $BASE_IMAGE; sleep 180; nova start $VM" &
