#! /bin/bash

# custom logstash connection
sed -i -e "s/#LS_HOST#/${LS_HOST}/g" /filebeat.yml
sed -i -e "s/#LS_PORT#/${LS_PORT}/g" /filebeat.yml
sed -i -e "s/#WORKER_COUNT#/${WORKER_COUNT:-5}/g" /filebeat.yml
exec /usr/sbin/filebeat -c /filebeat.yml -e -v
