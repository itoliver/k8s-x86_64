#!/bin/bash

# Make sure we're not confused by old, incompletely-shutdown
rm -rf /run/tomcat.pid
export PATH=$PATH:$CATALINA_HOME/bin

# overwrite portal server and port if provided via -e
sed -i -e "s/#PORTAL_SERVER#/${PORTAL_SERVER:-172.17.0.1}/g" $CATALINA_HOME/rm.conf
sed -i -e "s/#PORTAL_PORT#/${PORTAL_PORT:-8080}/g" $CATALINA_HOME/rm.conf
sed -i -e "s/#ENABLE_TENANT_POOL#/${ENABLE_TENANT_POOL:-false}/g" $CATALINA_HOME/rm.conf

exec catalina.sh run
