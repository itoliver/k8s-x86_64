## How to start docker?
Use command line like the following:
```sh
docker run -d  --name rmtest -p 8090:8080 -e PORTAL_SERVER=192.168.33.85  -e PORTAL_PORT=8080 cds/rm
```
where `PORTAL_SERVER` and `PORTAL_PORT` is the server address and port for HDS portal service.

