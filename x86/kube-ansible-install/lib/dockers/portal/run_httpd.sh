#!/bin/bash

# Make sure we're not confused by old, incompletely-shutdown httpd
# context after restarting the container.  httpd won't start correctly
# if it thinks it is already running.
rm -rf /run/httpd/* /tmp/httpd*

# link custom.py to correct one based on selected backend db engine
db=${DB_TYPE:-sqlite}
ln -sf "/data/CloudDB/custom.py.$db" /data/CloudDB/custom.py

# custom backend db connection
sed -i -e "s/#DBNAME#/${DB_NAME:-portal}/g" ./CloudDB/custom.py
sed -i -e "s/#USER#/${DB_USER:-cdstest}/g" ./CloudDB/custom.py
sed -i -e "s/#PASSWORD#/${DB_PWD:-passw0rd}/g" ./CloudDB/custom.py
sed -i -e "s/#HOST#/${DB_HOST:-db}/g" ./CloudDB/custom.py
sed -i -e "s/#PORT#/${DB_PORT:-}/g" ./CloudDB/custom.py
sed -i -e "s/#SERVER#/${DB_HOST:-db}/g" $INFORMIXSQLHOSTS # for informix only
nohup celery -A CloudDB worker -l info &
#  make sure permissions are correct
chown -R apache:apache /data/
exec /usr/sbin/apachectl -DFOREGROUND
