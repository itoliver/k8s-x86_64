## How to start?
- Start with port mapping and a name like the following:
```
docker run -d --name portal -p 8080:80 -e IDS_SERVER=172.16.1.158 -IDS_DB=portal -e IDS_USER=cdstest -e IDS_PWD=passw0rd cds/portal -e DB_TYPE=ifx
```
## How to init?
- Call _/init.sh/_ script: `docker exec portal /init.sh`
- Set up super user
```
docker exec -t=true -i=true portal /data/clouddb/manage.py \
    createsuperuser --username root --email zch@cn.ibm.com
```
