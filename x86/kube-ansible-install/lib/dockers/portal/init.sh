#!/usr/bin/env bash
rm -fr *.sqlite3
/data/manage.py migrate
/data/manage.py createsuperuser --username root --email zch@cn.ibm.com
chown -R apache:apache /data/
