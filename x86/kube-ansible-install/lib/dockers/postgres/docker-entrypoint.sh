#!/bin/bash
set -e

mkdir -p "$PGDATA"
chmod 700 "$PGDATA"
chown -R postgres "$PGDATA"

chmod g+s /run/postgresql
chown -R postgres /run/postgresql

# look specifically for PG_VERSION, as it is expected in the DB dir
if [ ! -s "$PGDATA"/PG_VERSION ]; then
	sudo -Eu postgres bash -c 'initdb "$POSTGRES_INITDB_ARGS"'

	# check password first so we can output the warning before postgres
	# messes it up
	if [ -n "$POSTGRES_PASSWORD" ]; then
		pass="PASSWORD '$POSTGRES_PASSWORD'"
		authMethod=md5
	else
		# The - option suppresses leading tabs but *not* spaces. :)
		cat >&2 <<-'EOWARN'
			****************************************************
			WARNING: No password has been set for the database.
			         This will allow anyone with access to the
			         Postgres port to access your database. In
			         Docker's default configuration, this is
			         effectively any other container on the same
			         system.

			         Use "-e POSTGRES_PASSWORD=password" to set
			         it in "docker run".
			****************************************************
		EOWARN

		pass=
		authMethod=trust
	fi

	{ echo; echo "host all all 0.0.0.0/0 $authMethod"; } >> "$PGDATA/pg_hba.conf"
	{ echo; echo "listen_addresses='*'"; } >> "$PGDATA/postgresql.conf"

	# internal start of server in order to allow set-up using psql-client		
	# does not listen on external TCP/IP and waits until start finishes

	sudo -Eu postgres bash -c 'pg_ctl -D "$PGDATA" \
		-o "-c listen_addresses='localhost'" \
		-w start'

	: ${POSTGRES_USER:=postgres}
	: ${POSTGRES_DB:=$POSTGRES_USER}
	export POSTGRES_USER POSTGRES_DB

	psql=( psql -v ON_ERROR_STOP=1 )

	if [ "$POSTGRES_DB" != 'postgres' ]; then
		"${psql[@]}" --username postgres <<-EOSQL
			CREATE DATABASE "$POSTGRES_DB" ;
		EOSQL
		echo
	fi

	if [ "$POSTGRES_USER" = 'postgres' ]; then
		op='ALTER'
	else
		op='CREATE'
	fi
	"${psql[@]}" --username postgres <<-EOSQL
		$op USER "$POSTGRES_USER" WITH SUPERUSER $pass ;
	EOSQL
	echo

	psql+=( --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" )

	echo

	sudo -Eu postgres bash -c 'pg_ctl -D "$PGDATA" -m fast -w stop'

	echo
	echo 'PostgreSQL init process complete; ready for start up.'
	echo
fi
exec sudo -Eu postgres bash -c "$@"
exec "$@"

