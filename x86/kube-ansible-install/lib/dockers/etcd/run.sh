#!/bin/sh
# Check for $CLIENT_URLS
local_ip=`ip addr show eth0 | perl -n -e'/inet (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/ && print $1'`
if [ -z ${CLIENT_URLS+x} ]; then
        CLIENT_URLS="http://${local_ip}:4001"
        echo "Using default CLIENT_URLS ($CLIENT_URLS)"
else
        echo "Detected new CLIENT_URLS value of $CLIENT_URLS"
fi

if [ -z ${LISTEN_CLIENT_URLS+x} ]; then
        LISTEN_CLIENT_URLS="http://${local_ip}:4001,http://127.0.0.1:4001"
        echo "Using default LISTEN_CLIENT_URLS ($LISTEN_CLIENT_URLS)"
else
        echo "Detected new LISTEN_CLIENT_URLS value of $LISTEN_CLIENT_URLS"
fi

# Check for $PEER_URLS
if [ -z ${PEER_URLS+x} ]; then
        PEER_URLS="http://${local_ip}:7001"
        echo "Using default PEER_URLS ($PEER_URLS)"
else
        echo "Detected new PEER_URLS value of $PEER_URLS"
fi

if [ -z ${NODE_NAME+x} ]; then
        NODE_NAME="default"
        echo "Using default node name:($NODE_NAME)"
else
        echo "Detected etcd name: ($NODE_NAME)"
fi

ETCD_CMD="/bin/etcd --name $NODE_NAME --data-dir=/data --initial-advertise-peer-urls=${PEER_URLS} \
--listen-peer-urls=${PEER_URLS} --listen-client-urls=${LISTEN_CLIENT_URLS} \
--advertise-client-urls=${CLIENT_URLS} --discovery=${DISCOVERY_URL}"
echo -e "Running '$ETCD_CMD'\nBEGIN ETCD OUTPUT\n"
$ETCD_CMD
