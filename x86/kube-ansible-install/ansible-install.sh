#!/bin/bash

cp ./roles/common/files/epel.repo /etc/yum.repos.d/

yum -y install ansible 

sed -i '/host_key_checking/ s/^#//' /etc/ansible/ansible.cfg

# The performance of the ascending ansible
sed -i '/forks/ s/^#\(.*\)5/\1 50/' /etc/ansible/ansible.cfg

sed -i '/#pipelining/ s/^#\(.*\)False/\1 True/' /etc/ansible/ansible.cfg
