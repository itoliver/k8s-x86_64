#!/bin/bash

EVENTS="CREATE,DELETE,MODIFY,MOVED_FROM,MOVED_TO"



sync() {
  rsync -togrvzp --delete --progress --include "dms-datactrl-tomcat" --include "iotdm1-tomcat" --include "iotdm2-tomcat" --include "rta-debug"  --include "rta-tomcat" --exclude '/*' /data/ftp/upload/longsys/* {{ ftp_server_ip }}::mnt

}
watch() {
  inotifywait -e "$EVENTS" -m -r --format '%:e %f' /data/ftp/upload/longsys/
}


watch | (
while true ; do
  read -t 1 LINE && sync
done
)
