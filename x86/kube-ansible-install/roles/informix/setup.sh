#! /bin/bash
#
# The script is for building base KVM iamge for HDS informix dynamic server nodes
# Here is something you can customize for different build:
#   - You can specify FILE_SERVER environment variable to point to another file server with customized bits
#   - You can specify INFORMIX_DIR to install ids to other directory
#   - You can specify ETCD_SERVER to use a different etcd cluster other than 172.16.1.45
#   - You can specify HDS_AZ to use a different available zone, default is ent (enterprize zone)
#   - You can specify IDS_TAR to use a different informix release other than ids_fc6.tar stored on $FILE_SERVER
#

set +x

RCol='\e[0m'    # Text Reset
Bla='\e[0;30m'; 
Red='\e[0;31m';
Gre='\e[0;32m';
Yel='\e[0;33m';
Blu='\e[0;34m';
Pur='\e[0;35m';
Cya='\e[0;36m';

INSTALL_DIR=~/base_setup
[ -d $INSTALL_DIR ] || mkdir $INSTALL_DIR
cd $INSTALL_DIR

function check_ret() {
    if [ $? != 0 ]; then
        echo -e ${Red}Failed: $*
    echo -e $RCol
        cd ~
        # rm -fr $INSTALL_DIR
        exit -1
    else
        echo -e ${Gre}Succeeded: $*
    echo -e $RCol
    fi
}

function start_step() {
    echo -e "${Cya}Deploy: $*"
    echo -e $RCol
}

function install_at9lib {
    [ -d /opt/at9.0/ ] || {
        start_step "install at9.0 lib64 so files"
        wget $FILE_SERVER/at9.0.lib64.tgz
        check_ret "download at9.0.lib64.tgz from file server"
        tar -xvf at9.0.lib64.tgz  -C /
        check_ret "extract at9.0.lib64.tgz to /opt/at9.0/"
    }

}



