---
# tasks file for informix

- include_vars: '{{ encrypt_vars_file }}'
  when: encrypt_vars_file is defined

- name: link elf lib
  file: src=/usr/lib64/libelf.so.1 dest=/usr/lib64/libelf.so state=link

- name: install utilities
  yum: name={{ item }} state=present
  with_items:
    - python-pip
    - python-devel
- name: upgrade pip
  pip:
    name: pip
    extra_args: '{{ pip_extra_args }} --upgrade'

- name: install required components
  yum: name={{ item }} state=present
  with_items:
    - net-snmp
    - net-snmp-libs
    - net-snmp-utils
    - net-snmp-python
    - keepalived

- name: configure keepalived
  copy: src=keepalived.conf dest=/etc/sysconfig/keepalived mode=0644

- name: create informix group
  group: name=informix state=present

- name: create informix user
  user: name=informix state=present shell=/bin/bash group=informix password={{ informix_password }}
  register: create_informix_user

- name: set operation user's password to never expire
  command: 'chage -I -1 -m 0 -M 99999 -E -1 informix'
  when: create_informix_user|changed

- name: check whether ids is installed
  stat: path={{ informix_dir }}/bin/oninit
  changed_when: false
  register: ids_ret

- name: create informix installation directory
  file: path={{ informix_dir }} owner=informix group=informix state=directory
  when: not ids_ret.stat.exists

- name: create temp installation folder
  file: path={{ informix_dir }}/setup/ids state=directory recurse=yes
  when: not ids_ret.stat.exists

- name: download informix installation package
  get_url: url={{ package_url }}/{{ informix_tar }} dest={{ informix_dir }}/setup checksum={{ informix_tar_checksum }}
  when: not ids_ret.stat.exists

- name: untar informix tar
  unarchive: src={{ informix_dir }}/setup/{{ informix_tar }} dest={{ informix_dir }}/setup/ids copy=no mode=0755
  when: not ids_ret.stat.exists

- name: install informix
  shell: './ids_install -i silent -DLICENSE_ACCEPTED=TRUE -DUSER_INSTALL_DIR={{ informix_dir }} && touch {{ informix_dir }}/installed.success'
  args:
    chdir: '{{ informix_dir }}/setup/ids'
    creates: '{{ informix_dir }}/installed.success'

- name: check whether client sdk is installed
  stat: path={{ informix_dir }}/bin/esql
  changed_when: false
  register: csdk_ret

- name: download informix csdk installation package
  get_url: url={{ package_url }}/clientsdk.tar dest={{ informix_dir }}/setup checksum={{ csdk_tar_checksum }}
  when: not csdk_ret.stat.exists

- name: create temp installation folder for csdk
  file: path={{ informix_dir }}/setup/csdk state=directory
  when: not csdk_ret.stat.exists

- name: untar csdk tar
  unarchive: src={{ informix_dir }}/setup/clientsdk.tar dest={{ informix_dir }}/setup/csdk copy=no mode=0755
  when: not csdk_ret.stat.exists

- name: install csdk
  shell: './installclientsdk -i silent -DLICENSE_ACCEPTED=TRUE -DUSER_INSTALL_DIR={{ informix_dir }} && touch {{ informix_dir }}/csdk_installed.success'
  args:
    chdir: '{{ informix_dir }}/setup/csdk'
    creates: '{{ informix_dir }}/csdk_installed.success'
  when: not csdk_ret.stat.exists

- name: configure ld for informix lib
  template: src=informix.ld.conf.j2 dest=/etc/ld.so.conf.d/informix.conf mode=0644
  notify: reload ldconfig

- name: download wire listener
  get_url: url={{ package_url }}/{{ item }} dest={{ informix_dir }}/setup force=yes
  with_items:
    - wl.tar

- name: install wire listener
  unarchive: src={{ informix_dir }}/setup/wl.tar dest={{ informix_dir }} owner=informix group=informix copy=no

- name: update sysctl and limts conf
  copy: src={{ item.src }} dest={{ item.dest }}
  with_items:
      - {src: sysctl.conf, dest: /etc/sysctl.conf}
      - {src: limits.conf, dest: /etc/security/limits.conf}

- name: create informix.sh
  template: src=informix.sh.j2 dest=/etc/profile.d/informix.sh mode=0644 force=no

- name: install informixdb and hds python packages
  pip: name={{ package_url }}/{{ item }} extra_args={{ pip_extra_args }}
  with_items:
    - InformixDB-2.5.tar.gz
    - 'hds-{{ hds_version }}-py2.py3-none-any.whl'

- name: create folder for ibm
  file: path=/opt/ibm owner=informix group=informix state=directory

- name: create folder for hds
  file: path=/opt/ibm/hds/logs owner=informix group=informix state=directory

- name: add initial HDS.ini
  template: src=HDS.ini.j2 dest=/opt/ibm/hds/HDS.ini owner=informix mode=0644 force=no

- name: prepare folder for allowed.surrogates
  file: path=/etc/informix/ state=directory owner=root group=informix

- name: create allowed.surrogates
  copy: src=allowed.surrogates dest=/etc/informix/allowed.surrogates owner=root group=informix mode=0644

- name: create a cron job for monitor
  cron: name="hds monitor" minute=*/3 job='su - -c "hds.monitor -t 200" >/dev/null 2>&1'
