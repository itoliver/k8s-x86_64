#!/bin/bash
#
echo 'Input to kill process type(PREMIUM | SHARING): '
read TYPE

if [[ "$TYPE" =~ PREMIUM|SHARING ]];then

    PROCESSNO=$(ps aux |grep java | grep $TYPE | awk '{print $2"---"$(NF-2)}')
    PROCESSNUMS=$(ps aux |grep java | grep $TYPE | awk '{print $2}' | wc -l)


    PS3='Choose to kill the tenantid sequence number("q" to exit)> '

    select processno in $PROCESSNO;do

        if [[ "$REPLY" =~ ^[0-9]+$ ]] && [ "$REPLY" -ge 1 -a "$REPLY" -le $PROCESSNUMS ];then
            echo -en "##########\\033[1;31m BEGIN \\033[0;39m##########";echo
            tenantid=$(echo $processno | cut -d'-' -f4)
            pid=$(echo $processno | cut -d'-' -f1)
            kill $pid && echo "killed $pid $tenantid"

        elif [[ "$REPLY" =~ ^57.*$ ]] && [[ ${#REPLY} == 24 ]];then
            echo -en "##########\\033[1;31m BEGIN \\033[0;39m##########";echo
            pid=$(ps aux |grep java | grep "$REPLY" | awk '{print $2}')
            kill $pid && echo "killed $pid $REPLY"

        elif [ "$REPLY" == 'q' ];then
            break
        else
            echo -en "##########\\033[1;31m BEGIN \\033[0;39m##########";echo
            echo 'Input error'
        fi
        REPLY=''
        echo;echo -en "##########\\033[1;31m  END  \\033[0;39m##########";echo

    done
else
    echo 'Input error'
fi

