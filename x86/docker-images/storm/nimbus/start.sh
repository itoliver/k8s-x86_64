#!/bin/sh

/usr/sbin/sshd -D &

/configure.sh ${ZOOKEEPER_SERVICE_HOST:-$1}

exec bin/storm nimbus
