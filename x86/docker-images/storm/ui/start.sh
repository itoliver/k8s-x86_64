#!/bin/sh

/usr/sbin/sshd -D &

/configure.sh ${ZOOKEEPER_SERVICE_HOST:-$1} ${NIMBUS_SERVICE_HOST:-$2}

exec bin/storm ui
