#!/bin/sh
if [ $# -gt 0 ]
then
    sh /opt/kafka_2.11-0.10.0.1/bin/kafka-topics.sh --describe --zookeeper kafka-zoo-svc:2181 --topic $1
else
    echo 'Usage: kafka-describe-topic <topicname>'
fi
