#!/bin/bash

cmd=$(which tmux) # tmux path
session=$1  # session name
TOPICNAME=${2:-DATA_SHARED_0} #topikname

if [ -z $cmd ]; then
  echo "You need to install tmux."
  exit 1
fi

if [[ $# < 1 ]];then
    echo; echo "Usage: $0 session"; echo;
    exit 1
fi

$cmd has -t $session
if [ $? != 0 ];then
  $cmd new-session -d -s $session
  #$cmd new-window -a -n kafka
  $cmd selectp -t $session
  $cmd split-window -h
  window=${session}:0
  pane=${window}.0
#  path=/home/yarn/longsys_debug/kafka
  path=/opt/kafka_2.11-0.10.0.1/bin
#  $cmd send-keys -t "$pane" "./produce.sh ${TOPICNAME}" Enter
  $cmd send-keys -t "$pane" "/opt/${path}/bin/kafka-console-producer.sh --broker-list kafka:9092 --topic ${TOPICNAME}" Enter
  $cmd select-pane -t "$pane"
  pane1=${window}.1
#  $cmd send-keys -t "$pane1" "./consume.sh ${TOPICNAME}" Enter
  $cmd send-keys -t "$pane1" "/opt/${path}/bin/kafka-console-consumer.sh --zookeeper kafka-zoo-svc:2181 --topic ${TOPICNAME}" Enter
  $cmd select-pane -t "$pane"
  $cmd select-window -t "$window"
fi

$cmd attach-session -t "$session"

exit 0

