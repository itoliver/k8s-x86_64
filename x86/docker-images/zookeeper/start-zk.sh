#!/bin/bash

/usr/sbin/sshd -D &

sed -i -r 's|#(log4j.appender.ROLLINGFILE.MaxBackupIndex.*)|\1|g' $ZK_HOME/conf/log4j.properties
sed -i -r 's|#autopurge|autopurge|g' $ZK_HOME/conf/zoo.cfg
sed -i -r 's|tickTime=2000|tickTime=3000|g' $ZK_HOME/conf/zoo.cfg
/opt/zookeeper-3.4.9/bin/zkServer.sh start-foreground
