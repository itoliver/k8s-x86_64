#!/bin/bash


if [[ $# < 1 ]];then
    echo; echo "Usage: $0 log_dir <line_numbers>"; echo;
    exit 1
fi

type_prompt='Select System Type(type 'q' exit)> '
PS3="${type_prompt}"

file_choices=${1}/*

LINENUM=${2:-10}

select FILE in ${file_choices} QUIT
do
    if [ $REPLY == 'q' ];then
        break
    fi

    if [ -e $FILE ] ; then
        echo -en "##########\\033[1;31m BEGIN \\033[0;39m##########";echo
        tail -n$LINENUM $FILE
    else
        break
    fi

    REPLY=''
    echo;echo -en "##########\\033[1;31m  END  \\033[0;39m##########";echo
done

